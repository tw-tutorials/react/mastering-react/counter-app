import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {
    render() {
        const { counters, onReset, onDelete, onIncrement, onDecrement } = this.props;

        return (
            <div>
                <button className="btn primary btn-sm m-2" onClick={onReset}>Reset</button>
                {counters.map(counter =>
                    <React.Fragment key={counter.id}>
                        <br />
                        <Counter
                            counter={counter}
                            onDelete={onDelete}
                            onIncrement={onIncrement}
                            onDecrement={onDecrement}
                        />
                    </React.Fragment>)}
            </div>
        );
    }
}

export default Counters;